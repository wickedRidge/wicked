<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="<?= assets(); ?>/stylesheets/master.css">
    </head>
    <body>
        <header>
            Website Header
        </header>
        <main>
            <?php
                $engine = new Wicked\TemplateEngine($this);
            ?>
        </main>
        <footer>
            Website footer
        </footer>
    </body>
</html>