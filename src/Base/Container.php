<?php


namespace Wicked\Base;

use Closure;
use Exception;
use ReflectionClass;
use ReflectionParameter;

class Container {

    public static $registry = [];

    protected static $shared = [];

    // Register
    public static function register($name, Closure $resolve)
    {
        static::$registry[$name] = $resolve();
    }

    // Singleton
    public static function singleton($name, Closure $resolve)
    {
        static::$shared[$name] = $resolve();
    }

    // Resolve, consider register or singleton here
    public static function resolve($name)
    {

        if (static::isRegistered($name))
        {
            $name = static::$registry[$name];

            return $name();
        }
        if (static::isSingleton($name))
        {
            $instance = static::$shared[$name];

            return $instance;
        }

        throw new Exception('Nothing registered with that name, fool.');
    }

    // Check resigtered or not
    public static function isRegistered($name)
    {
        return array_key_exists($name, static::$registry);
    }

    // Check singleton object or not
    public static function isSingleton($name)
    {
        return array_key_exists($name, static::$shared);
    }

    public function make($class, $parameters = [])
    {
        $reflector = new ReflectionClass($class);


        $constructor = $reflector->getConstructor();

        if(is_null($constructor)) return new $class;

        $dependencies = $constructor->getParameters();

        $parameters = $this->keyParametersByArgument($dependencies, $parameters);

        $instance = $this->getDependencies($dependencies, $parameters);

        return $reflector->newInstanceArgs($instance);
    }

    /**
     * Resolve all of the dependencies from the ReflectionParameters.
     *
     * @param  array $parameters
     * @param  array $primitives
     *
     * @return array
     */
    protected function getDependencies($parameters, array $primitives = [])
    {
        $dependencies = [];

        foreach ($parameters as $parameter)
        {
            $dependency = $parameter->getClass();

            if (array_key_exists($parameter->name, $primitives))
            {
                $dependencies[] = $primitives[$parameter->name];
            }
            elseif (is_null($dependency))
            {
                $dependencies[] = $this->resolveNonClass($parameter);
            }
            else
            {
                $dependencies[] = $this->resolveClass($parameter);
            }
        }

        return (array) $dependencies;
    }

    /**
     * If extra parameters are passed by numeric ID, rekey them by argument name.
     *
     * @param  array  $dependencies
     * @param  array  $parameters
     * @return array
     */
    protected function keyParametersByArgument(array $dependencies, array $parameters)
    {
        foreach ($parameters as $key => $value)
        {
            if (is_numeric($key))
            {
                unset($parameters[$key]);

                $parameters[$dependencies[$key]->name] = $value;
            }
        }

        return $parameters;
    }

    /**
     * Resolve a non-class hinted dependency.
     *
     * @param  \ReflectionParameter  $parameter
     * @return mixed
     *
     * @throws Exception
     */
    protected function resolveNonClass(ReflectionParameter $parameter)
    {
        if ($parameter->isDefaultValueAvailable())
        {
            return $parameter->getDefaultValue();
        }

        $message = "Unresolvable dependency resolving [$parameter] in class {$parameter->getDeclaringClass()->getName()}";

        throw new Exception($message);
    }

    /**
     * Resolve a class based dependency from the container.
     *
     * @param  \ReflectionParameter  $parameter
     * @return mixed
     *
     * @throws Exception
     */
    protected function resolveClass(ReflectionParameter $parameter)
    {
        try
        {
            return $this->make($parameter->getClass()->name);
        }

            // If we can not resolve the class instance, we will check to see if the value
            // is optional, and if it is we will return the optional parameter value as
            // the value of the dependency, similarly to how we do this with scalars.
        catch (Exception $e)
        {
            if ($parameter->isOptional())
            {
                return $parameter->getDefaultValue();
            }

            throw $e;
        }
    }

}