<?php


namespace Wicked\Base;

use Wicked\Database;
use Wicked\Helpers\Config;
use Wicked\Http\Request;
use Wicked\Http\Router;
use Wicked\QueryBuilder;
use Wicked\Sessions\Encrypter;
use Wicked\Sessions\MySQLSessionHandler;
use Wicked\Sessions\Session;

class App extends Container
{

    public function __construct()
    {
        $this->singleton('config', function ()
        {
            return new Config();
        });

        $this->singleton('database', function ()
        {
           return new Database($this->resolve('config'));
        });

        $this->singleton('session', function ()
        {
            $db = ($this->resolve('database'));
            $queryBuilder =  new QueryBuilder($db->connection());
            $queryBuilder->setTable('sessions');
            return new Session(new MySQLSessionHandler(new Encrypter('SomeKeyValuePair'), $queryBuilder));
        });

        $this->singleton('request', function ()
        {
            return new Request();
        });

        $this->singleton('router', function ()
        {
            return new Router();
        });


        $router = $this->resolve('router');
        $router->getRoutes();
        $request = $this->resolve('request');

        $router->handleRequest($request, $this);

    }

}