<?php

use Wicked\Base\Container;

function assets()
{
    $config = Container::resolve('config');
    return $config->get('paths.assets');
}

function root()
{
    return dirname($_SERVER['SCRIPT_FILENAME']);
}

function session($key, $value = false)
{
    $session = Container::resolve('session');
    if($value)
    {
         $session->set($key, $value);
    }
    else
    {
        return $session->get($key);
    }
}
