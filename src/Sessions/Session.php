<?php namespace Wicked\Sessions;

use Wicked\Helpers\ArrayHelper;
use SessionHandlerInterface;

class Session
{

    /**
     * The Session Handler to use
     *
     * @var \SessionHandlerInterface
     */
    private $handler;

    public function __construct(SessionHandlerInterface $handler)
    {
        $this->handler = $handler;
        session_set_save_handler($this->handler, true);
    }

    /**
     * Set a Session value by key
     *
     * @param $key
     * @param $value
     */
    public function set($key, $value)
    {
        $this->startSession();
        ArrayHelper::set($_SESSION, $key, $value);
    }

    /**
     * Get a Session value by key
     *
     * @param $key
     * @return mixed|null
     */
    public function get($key)
    {
        if($this->has($key))
        {
            return ArrayHelper::get($_SESSION, $key);
        }
        return null;
    }

    /**
     * Check if this Session has some key
     *
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        $this->startSession();
        return ArrayHelper::has($_SESSION, $key);
    }

    /**
     * Start or resume a Session if it isn't active
     */
    private function startSession()
    {
        if(session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
    }

}