<?php

namespace Wicked\Sessions;

use SessionHandlerInterface;
use Wicked\QueryBuilder;

class MySQLSessionHandler implements SessionHandlerInterface
{

    /**
     * The Session Handler's encrypter
     *
     * @var \Wicked\Sessions\Encrypter
     */
    private $encrypter;

    /**
     * The query builder to execute on
     *
     * @var \Wicked\QueryBuilder
     */
    private $db;

    /**
     * The max life of the Session
     *
     * @var string
     */
    private $lifetime;

    /**
     * Check if this Session is already running
     *
     * @var bool
     */
    private $exists;

    public function __construct(Encrypter $encrypter, QueryBuilder $db, $lifetime = null)
    {
        $this->db = $db;
        $this->encrypter = $encrypter;
        $this->lifetime = $lifetime;
    }

    /**
     * Read data from the Session with id
     *
     * @param string $id
     * @return string
     */
    public function read($id)
    {
        $data = $this->db->select('data')->where('id', '=', $id)->get();

        if ($data)
        {
            $this->exists = true;

            return $this->encrypter->decrypt($data['data']);
        }
        else
        {
            return '';
        }
    }

    /**
     * Write data to the Session with id
     *
     * @param string $id
     * @param string $data
     *
     * @return void
     */
    public function write($id, $data)
    {
        $data = $this->encrypter->encrypt($data);
        if ($this->exists)
        {
            $this->db->update(['data', 'last_accessed'], [$data, time()])->where('id', '=', $id)->fire();
        }
        else
        {
            $this->db->insert(['data', 'last_accessed', 'id'], [$data, time(), $id])->fire();
        }

        $this->exists = true;
    }

    /**
     * Close the session
     *
     * @return bool
     */
    public function close()
    {
        if ($this->db->close())
        {
            return true;
        }

        return false;
    }

    /**
     * Destroy the current Session with id
     *
     * @param string $id
     * @return bool
     */
    public function destroy($id)
    {
        $this->unsetCookies();
        if ($destroy = $this->db->delete()->where('id', '=', $id)->fire())
        {
            return true;
        }

        return false;
    }

    /**
     * Garbage collection
     *
     * @param int $lifetime
     *
     * @return void
     */
    public function gc($lifetime)
    {
        $old = time() - (is_null($this->lifetime)) ? $lifetime : $this->lifetime;
        $this->unsetCookies();
        $this->db->delete()->where('last_accessed', '<', $old)->fire();
    }

    /**
     * Open the session
     *
     * @param string $savePath
     * @param string $sessionName
     * @return bool
     */
    public function open($savePath, $sessionName)
    {
        return true;
    }

    /**
     * Unset the Cookies from the browser.
     */
    private function unsetCookies()
    {
        if (ini_get("session.use_cookies"))
        {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
    }

}