<?php namespace Wicked;

use Wicked\Helpers\Config;

class Database {

    /**
     * The PDO instance
     *
     * @var \PDO
     */
    private $pdo;

    /**
     * The database driver to be used
     *
     * @var string
     */
    private $driver;

    /**
     * The database host
     *
     * @var string
     */
    private $host;

    /**
     * The database port
     *
     * @var string
     */
    private $port;

    /**
     * The username to access the database
     *
     * @var string
     */
    private $username;

    /**
     * The password to access the database
     *
     * @var string
     */
    private $password;

    /**
     * The database name
     *
     * @var string
     */
    private $database;


    public function __construct(Config $config)
    {
        $this->driver = $config->get('database.driver');
        $this->host = $config->get('database.host');
        $this->port = $config->get('database.port');
        $this->username = $config->get('database.username');
        $this->password = $config->get('database.password');
        $this->database = $config->get('database.database');
        $this->connect();
    }

    /**
     * Try to connect to the database.
     *
     * @throws \PDOException if the PDO instance cannot be created
     */
    private function connect()
    {
        try
        {
            $this->pdo = new \PDO($this->generateDNS(), $this->username, $this->password);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $error)
        {
            print get_class($error) . ' ' . $error->getMessage();
        }

    }

    /**
     * Returns the DNS string to be used for creating the PDO instance.
     *
     * @return string
     */
    private function generateDNS()
    {
        return $this->driver . ':host=' . $this->host . ';port=' . $this->port . ';dbname=' . $this->database;
    }

    /**
     * Returns the PDO connection
     *
     * @return \PDO
     */
    public function connection()
    {
        return $this->pdo;
    }

}