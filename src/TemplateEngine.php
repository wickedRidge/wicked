<?php

namespace Wicked;

use Wicked\Helpers\ArrayHelper;

class TemplateEngine {

    /**
     * The current template
     *
     * @var string
     */
    private $template;

    /**
     * Array of View variables
     *
     * @var array
     */
    private $variables = [];

    public function __construct(View $view)
    {
        if (file_exists($view->viewName) && is_readable($view->viewName))
        {
            $this->template = $view->viewName;
        }
        if ($view->isNotEmpty())
        {
            $this->setAll($view);
        }
        $this->render();
    }

    /**
     * Get a variable by key
     *
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return ArrayHelper::get($this->variables, $key);
    }

    /**
     * Set a variable by key
     *
     * @param $key
     * @param $value
     */
    public function set($key, $value)
    {
        $this->variables[$key] = $value;
    }

    /**
     * Render the template and then render the capture contents
     */
    public function render()
    {
        extract($this->variables);
        chdir(dirname($this->template));
        ob_start();

        require basename($this->template);

        $content = preg_replace("/^&incorporates\(\'.+\'\)/", "", ob_get_clean());

        echo $content;
    }

    /**
     * Create variables to be used in the template view
     *
     * @param \Wicked\View $view
     */
    private function setAll(View $view)
    {
        $this->variables = $view->all();
        foreach($this->variables as $variable => $value)
        {
            ${$variable} = $value;
        }
    }
}