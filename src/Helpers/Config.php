<?php namespace Wicked\Helpers;

class Config {

    /**
     * The config director
     *
     * @var array|bool|string
     */
    private $directory = [];

    /**
     * An array of config data
     *
     * @var array
     */
    private $data = [];

    /**
     * The default return value
     *
     * @var null
     */
    private $default = null;

    public function __construct($directory = false)
    {
        $this->directory = ($directory === false) ?  'config' : $directory;
        $this->loadDir();
    }

    /**
     * Get the key from the config data using "." notation
     *
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        $this->default = $default;

        $data = ArrayHelper::get($this->data, $key, $default);

        return $data;
    }

    /**
     * Load the data from the config director to be accessed
     */
    private function loadDir()
    {
        foreach(glob($this->directory . '/*.php') as $file)
        {
            if(count(require $file))
            {
                $this->data[basename($file, ".php")] = require $file;
            }
        }
    }
}