<?php

function e($value)
{
    return htmlentities($value, ENT_QUOTES, 'UTF-8', false);
}

function killDump($data)
{
    (new Wicked\Helpers\Dumper\Dumper())->killDump($data);
}

function pp($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    exit;
}

function getCaller()
{
    $trace = debug_backtrace();
    $name = $trace[2]['function'];
    return empty($name) ? 'global' : $name;
}