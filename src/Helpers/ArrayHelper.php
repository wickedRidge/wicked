<?php namespace Wicked\Helpers;

use Closure;

class ArrayHelper {

    /**
     * Set value in array using dot notation
     *
     * @param $array
     * @param $key
     * @param $value
     * @return mixed
     */
    public static function set(&$array, $key, $value)
    {
        if (is_null($key)) return $array = $value;

        $keys = explode('.', $key);

        while (count($keys) > 1)
        {
            $key = array_shift($keys);

            // If the key doesn't exist at this depth, we will just create an empty array
            // to hold the next value, allowing us to create the arrays to hold final
            // values at the correct depth. Then we'll keep digging into the array.
            if ( ! isset($array[$key]) || ! is_array($array[$key]))
            {
                $array[$key] = [];
            }

            $array =& $array[$key];
        }

        $array[array_shift($keys)] = $value;

        return $array;
    }
    /**
     * Get value at key of array using dot notation
     *
     * @param $array
     * @param $key
     * @param null $default
     *
     * @return mixed
     */
    public static function get($array, $key, $default = null)
    {
        if (is_null($key)) return $array;

        if (isset($array[$key])) return $array[$key];

        foreach (explode('.', $key) as $segment)
        {

            if (!is_array($array) || !array_key_exists($segment, $array))
            {

                return self::value($default);
            }

            $array = $array[$segment];
        }

        return $array;
    }

    /**
     * Check if array has index using dot notation
     *
     * @param $array
     * @param $key
     *
     * @return bool
     */
    public static function has($array, $key)
    {
        if (empty($array) || is_null($key)) return false;

        if (array_key_exists($key, $array)) return true;

        foreach (explode('.', $key) as $segment)
        {
            if ( ! is_array($array) || ! array_key_exists($segment, $array))
            {
                return false;
            }

            $array = $array[$segment];
        }

        return true;
    }

    private static function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }

    /**
     * Get max or min value in the array
     *
     * @param $array
     * @param $key
     * @param string $operator
     *
     * @return mixed|null
     */
    public static function compare($array, $key, $operator = '>')
    {
        if(!self::has($array, $key)) return null;
        return array_reduce($array, function ($a, $b) use($key, $operator) {
            if($operator === '>')
                return $a[$key] > $b[$key] ? $a : $b;
            else
                return $a[$key] < $b[$key] ? $a : $b;
        });
    }

}