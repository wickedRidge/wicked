<?php namespace Wicked\tests;

use PHPUnit_Framework_TestCase;
use Wicked\Http\Message\Uri;

class UriTest extends PHPUnit_Framework_TestCase {

    public function testCanMakeParts()
    {
        $uri = new Uri('https://');

        $this->assertEquals('https', $uri->filterScheme($uri->getScheme()));
    }

}