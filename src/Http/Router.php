<?php namespace Wicked\Http;

use Wicked\Base\App;
use Wicked\Helpers\ArrayHelper;
use Wicked\Helpers\StringHelper;
use Wicked\Http\Exceptions\RouteRequestMethodMismatchException;

class Router implements \Countable
{

    /**
     * An array of routes keyed by uri
     *
     * @var array
     */
    private $routes = [];

    /**
     * The default redirect path
     *
     * @var string
     */
    private $defaultPath = 'home';

    /**
     * An array of acceptable HTTP verbs
     *
     * @var array
     */
    public static $verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];


    /**
     * Adds a route to the array of routes
     *
     * @param $uri
     * @param $method
     * @param $controller
     * @param $action
     */
    public function addRoute($uri, $method, $controller, $action)
    {
        $route = new Route($uri, $method, $controller, $action);
        ArrayHelper::set($this->routes, $route->getUri() . '.' . $method, $route);
    }

    /**
     * Handles the incoming request or redirects home
     *
     * @param Request $request
     * @param App $app
     */
    public function handleRequest(Request $request, App $app)
    {
        $routeDotMethod = $request->getUri() . '.' . $request->getMethod();
        if (in_array($request->getMethod(), self::$verbs) && $this->hasRoute($routeDotMethod))
        {
            $route = $this->getRoute($routeDotMethod);
            try
            {
                $this->matchRouteAndRequestMethods($route->getMethod(), $request->getMethod());
                $controller = $app->make('Controllers\\' . $route->getController());
                $this->routeRequest($controller, $route);
            } catch(RouteRequestMethodMismatchException $error)
            {
                print get_class($error) . ' ' . $error->getMessage();
            }
        }
        else
        {
            $redirect = new Redirect();
            $redirect->to($this->getDefaultPath());
        }
    }

    /**
     * Check if a route with the given uri "." method exists
     *
     * @param $routeDotMethod
     * @return bool
     */
    public function hasRoute($routeDotMethod)
    {
        return ArrayHelper::has($this->routes, $routeDotMethod);
    }

    /**
     * Get the route with this uri "." method
     *
     * @param $routeDotMethod
     * @return \Wicked\Http\Route
     */
    public function getRoute($routeDotMethod)
    {
        return ArrayHelper::get($this->routes, $routeDotMethod);
    }

    /**
     * Gets the registerd routes from the routes file
     */
    public function getRoutes()
    {
        require(root() . '/Controllers/routes.php');
    }

    /**
     * Sets the default path if the route has been registered
     * @param $newDefaultPath
     */
    public function setDefaultPath($newDefaultPath)
    {
        if ($this->hasRoute($newDefaultPath))
        {
            $this->defaultPath = $newDefaultPath;
        }
    }

    /**
     * Get the default path
     *
     * @return string
     */
    private function getDefaultPath()
    {
        return $this->defaultPath;
    }

    //docket match request and route
    /**
     * Routes the given route to the controller action
     *
     * @param Controller $controller
     * @param Route $route
     */
    public function routeRequest(Controller $controller, Route $route)
    {
        $action = $route->getAction();
        $parameters = $controller->getMethodParams($action, $route->parameterNames());
        if (!empty($parameters))
        {
            $controller->callAction($action, $parameters);
        }
        else
        {
            $controller->callAction($action);
        }
    }

    private function matchRouteAndRequestMethods($routeMethod, $requestMethod)
    {
        if(StringHelper::equals($routeMethod, $requestMethod))
        {
            return true;
        }
        throw new RouteRequestMethodMismatchException;
    }

    /**
     * Returns the number of routes
     *
     * @return int
     */
    public function count()
    {
        return count($this->routes);
    }

}