<?php namespace Wicked\Http;

use Wicked\View;

class Controller {

    /**
     * The View object used by all controllers
     *
     * @var View
     */
    public $view;

    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * Calls the Controller's method with optional parameters.
     *
     * @param $action
     * @param array $params
     */
    public function callAction($action, $params = [])
    {
        call_user_func_array([$this, $action], $params);
    }

    /**
     * Get the Controller's method parameters.
     *
     * Allows for Dependency Injection in Controller methods
     *
     * @param $method
     * @param $parameters
     * @return array
     */
    public function getMethodParams($method, $parameters)
    {
        $methodReport = new \ReflectionMethod($this, $method);
        foreach ($methodReport->getParameters() as $key => $parameter)
        {
            $class = $parameter->getClass();
            if (!is_null($class))
            {
                array_splice($parameters, $key, 0, [new $class->name]);
            }
            else
            {
                $parameters[$key] = null;
            }
        }

        //docket argument/parameter order matters
        return $parameters;
    }

    /**
     * Check if this Controller has a method
     *
     * @param $method
     *
     * @return bool
     */
    public function hasMethod($method)
    {
        return method_exists($this, $method);
    }

    /**
     * Resolve the Controller for the given Route
     *
     * @param Route $route
     * @return \Wicked\Http\Controller
     */
    public static function resolveController(Route $route)
    {
        $controllerName = 'Controllers\\' . $route->getController();

        return new $controllerName();
    }
}