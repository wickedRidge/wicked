<?php


namespace Wicked\Http;

class Route {

    private $uri;
    private $method;
    private $controller;
    private $action;
    private $params;
    private $parameterNames;

    public function __construct($uri, $method, $controller, $action)
    {
        $this->uri = $this->parseUri($uri);
        $this->method = $method;
        $this->controller = $controller;
        $this->action = $action;
        $this->params = [];
        $this->parameterNames = [];
    }

    private function parseUri($uri)
    {
        $uri = trim($uri, '/');
        if($this->containsParameters($uri))
        {
            $this->parameterNames($uri);
        }
        return $uri;
    }

    private function containsParameters($uri)
    {
        $query = false;
        preg_match('/{\$([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)}/', $uri, $query);
        return $query;
    }

    /**
     * Get this Route's Uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Get this Route's server request method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }


    /**
     * Get this Route's registered controller
     *
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Get this Route's
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    public function setParameters($params)
    {
        $this->params = $params;
    }

    public function setParameter($value, $key = null)
    {
        $this->params[] = $value;
    }

    public function getParameters()
    {
        return ($this->hasParameters()) ? $this->params : [];
    }

    public function getParameter($name)
    {
        return $this->params[$name];
    }

    public function hasParameter($name)
    {
        return in_array($name, $this->params);
    }

    public function hasParameters()
    {
        return (!empty($this->params));
    }

    public function parameterNames($uri = false)
    {
        if(isset($this->parameterNames)) return $this->parameterNames;

        return $this->parameterNames = $this->compileParameterNames($uri);
    }

    private function compileParameterNames($uri)
    {
        preg_match_all('/\{(.*?)\}/', $uri, $matches);

        return array_map(function($m) { return trim($m, '?'); }, $matches[1]);
    }

}