<?php

namespace Wicked\Http\Exceptions;

use Exception;

class RouteRequestMethodMismatchException extends Exception {

    /**
     * Create a new \Wicked\Http\Exceptions\RouteRequestMethodMismatchException
     *
     * @param null $error
     */
    public function __construct($error = null)
    {
        if (empty($error)) {
            $error = "The route and request doesn't match";
        }
        parent::__construct($error);
    }
}