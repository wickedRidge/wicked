<?php namespace Wicked\Http;

class Redirect
{
    /**
     * Redirect to the given url with the given status
     *
     * @param $url
     * @param int $status
     */
    public function to($url, $status = 302)
    {
        $response = new Response(sprintf('<!DOCTYPE html>
        <html>
            <head>
                <meta charset="UTF-8" />
                <meta http-equiv="refresh" content="1;url=%1$s" />

                <title>Redirecting to %1$s</title>
            </head>
            <body>
                Redirecting to <a href="%1$s">%1$s</a>.
            </body>
        </html>', htmlspecialchars($url, ENT_QUOTES, 'UTF-8')), $status, ["Location" => "/$url"]);

        $response->sendHeaders();
    }


}