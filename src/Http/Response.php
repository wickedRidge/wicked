<?php namespace Wicked\Http;

class Response {

    /**
     * An array of HTTP status codes
     *
     * @var array
     */
    public static $statusTexts = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',            // RFC2518
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',          // RFC4918
        208 => 'Already Reported',      // RFC5842
        226 => 'IM Used',               // RFC3229
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Reserved',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',    // RFC7238
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',                                               // RFC2324
        422 => 'Unprocessable Entity',                                        // RFC4918
        423 => 'Locked',                                                      // RFC4918
        424 => 'Failed Dependency',                                           // RFC4918
        425 => 'Reserved for WebDAV advanced collections expired proposal',   // RFC2817
        426 => 'Upgrade Required',                                            // RFC2817
        428 => 'Precondition Required',                                       // RFC6585
        429 => 'Too Many Requests',                                           // RFC6585
        431 => 'Request Header Fields Too Large',                             // RFC6585
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates (Experimental)',                      // RFC2295
        507 => 'Insufficient Storage',                                        // RFC4918
        508 => 'Loop Detected',                                               // RFC5842
        510 => 'Not Extended',                                                // RFC2774
        511 => 'Network Authentication Required',                             // RFC6585
    ];

    /**
     * Response content
     *
     * @var string
     */
    private $content;

    /**
     * The Response status code
     * @var int
     */
    private $status;

    /**
     * An array of Response headers
     *
     * @var array
     */
    private $headers = [];

    public function __construct($content = '', $status = 200, $headers = [])
    {
        $this->content = (string) $content;
        $this->status = (int) $status;
        if (!empty($headers)) $this->addHeaders($headers);
    }

    /**
     * Add multiple headers to the Response's header array
     *
     * @param $headers
     */
    public function addHeaders($headers)
    {
        foreach ($headers as $key => $values)
        {
            $this->addHeader($key, $values);
        }
    }

    /**
     * Add a header to the Response's header array
     *
     * @param $name
     * @param $value
     */
    public function addHeader($name, $value){
        $this->headers[$name][] = $value;
    }

    /**
     * Set the header's value by name
     *
     * @param $name
     * @param $value
     */
    public function setHeader($name, $value){
        $this->headers[$name] = [
            (string) $value,
        ];
    }

    /**
     * Send the Response's headers
     *
     * @return \Wicked\Http\Response
     */
    public function sendHeaders()
    {
        if(headers_sent())
        {
            return $this;
        }

        // status
        header(sprintf('HTTP/%s %s %s', '1.1', $this->status, self::$statusTexts[$this->status]), true, $this->status);

        // headers
        foreach ($this->headers as $name => $values) {
            foreach ($values as $value) {
                header($name.': '.$value, false, $this->status);
            }
        }

        return $this;
    }

    /**
     * Determine if this Response is a redirect
     *
     * @param null $location
     * @return bool
     */
    public function isRedirect($location = null)
    {
        return in_array($this->status , array(201, 301, 302, 303, 307, 308)) && (null === $location ?: $location == $this->headers['Location']);
    }

    /**
     * Set the Response's status code
     *
     * @param int $status
     * @return \Wicked\Http\Response
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
}