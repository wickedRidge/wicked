<?php namespace Wicked\Http;

use Wicked\Helpers\ArrayHelper;

class Request {

    /**
     * Contents of the Host: header from the current request, if there is one.
     *
     * @var string
     */
    private $host;

    /**
     * The request uri
     *
     * @var string
     */
    private $uri;

    /**
     * The request method
     *
     * @var string
     */
    private $method;

    /**
     * The request headers
     *
     * @var array
     */
    private $headers;

    /**
     * An array of variables retrieved from the request, if there are any
     *
     * @var array
     */
    private $variables;

    //docket header content type XSS
    public function __construct()
    {
        $this->host = $_SERVER['HTTP_HOST'];
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->headers = [];
        $this->variables = [];
        //print_r(get_headers('http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"));
        $this->parseUri();
        $this->setVariables();
    }

    /**
     * Remove the the beginning and ending forward slash
     */
    private function parseUri()
    {
        $this->uri = trim($this->uri, '/');
    }

    /**
     * Set the Request uri
     *
     * @param $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
        $this->parseUri();
    }

    /**
     * Get the Request uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set the Request method
     *
     * @param $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * Get the Request method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set an array of variables from the
     * Request's SERVER variables
     */
    private function setVariables()
    {
        switch ($this->method)
        {
            case 'GET':
                $this->variables = $_GET;
                unset($this->variables['url']);
                break;
            case 'POST':
                $this->variables = $_POST;
                break;
        }
    }

    /**
     * Get the Request's variables
     *
     * @return array
     */
    public function all()
    {
        return $this->variables;
    }

    /**
     * Get a Request variable by key
     *
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return ArrayHelper::get($this->variables, $key);
    }

}