<?php

namespace Wicked;

use Wicked\Helpers\ArrayHelper;
use Wicked\Http\Redirect;
use Wicked\Helpers\StringHelper;
use Wicked\Helpers\Config;

class View {

    /**
     * This View's variables
     *
     * @var array
     */
    private $variables;

    public function __construct()
    {
        $this->variables = [];
    }


    /**
     * Render the View if it does not incorporate a template
     *
     * @param $viewName
     * @return \Wicked\View
     */
    public function render($viewName)
    {
        if (!$this->incorporates($this->getViewFileName($viewName)))
        {
            require($this->getViewFileName($viewName));
        }
        return $this;
    }

    /**
     * Get this View's variables
     *
     * @return array
     */
    public function all()
    {
        return $this->variables;
    }

    /**
     * Render this View with some key value pair
     *
     * @param $key
     * @param null $value
     * @return \Wicked\View
     */
    public function with($key, $value = null)
    {
        if (is_array($key))
        {
            $this->variables = array_merge($this->variables, $key);
        }
        else
        {
            $this->variables[$key] = $value;
        }
        return $this;
    }

    /**
     * Check to see if this View has some key in its variables
     *
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        return ArrayHelper::has($this->variables, $key);
    }

    /**
     * Check to see if this View has any variables
     *
     * @return bool
     */
    public function isNotEmpty()
    {
        return !empty($this->variables);
    }

    /**
     * Returns this View's path
     *
     * @param $viewName
     * @return string
     */
    private function getViewFileName($viewName)
    {
        return ('Views/' . $viewName . '.php');
    }

    /**
     * Incorporate the given template
     *
     * @param $file
     * @return bool
     */
    private function incorporates($file)
    {
        $firstLine = fgets(fopen($file, 'r'));

        if (StringHelper::startsWith($firstLine, '&incorporates'))
        {
            preg_match('~(["\'])([^"\']+)\1~', trim($firstLine), $layout);
            $this->viewName = $file;
            $layout = $this->getViewFileName($layout[2]);
            if(file_exists($layout) && is_readable($layout))
            {
                require($layout);
                return true;
            }
            else
            {
                new Redirect('error');
            }
        }

        return false;
    }
}