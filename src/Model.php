<?php

namespace Wicked;

use Wicked\Base\Container;

class Model extends QueryBuilder {

    /**
     * The Database being used
     *
     * @var \Wicked\Database
     */
    private $db;

    /**
     * The current table name for this Model
     *
     * @var string
     */
    protected $table;

    public function __construct()
    {
        $this->db = Container::resolve('database');
        parent::__construct($this->db->connection());
        $this->setTable($this->table());
    }

    /**
     * Returns the current table for this Model
     *
     * @return string
     */
    public function table()
    {
        return $this->table;
    }
}