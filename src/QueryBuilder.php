<?php

namespace Wicked;

use Wicked\Helpers\StringHelper;

class QueryBuilder
{

    /**
     * The Database connection used for queries
     *
     * @var \PDO
     */
    private $connection;

    /**
     * The current query
     *
     * @var string
     */
    private $query;

    /**
     * The current bindings for this query
     *
     * @var array
     */
    private $bindings = [];

    /**
     * The table being queried
     *
     * @var string
     */
    private $table;

    /**
     * The fetch mode to be used
     * Default fetch by column name
     *
     * @var int
     */
    private $fetchMode = \PDO::FETCH_ASSOC;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Set the current table
     *
     * @param $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * Get everything from the current table
     *
     * @return mixed
     */
    public function all()
    {
        return $this->select('*')->get();
    }

    /**
     * Start a Select statement with the given fields
     *
     * @param $fields
     * @return $this
     */
    public function select($fields)
    {
        $this->query = "SELECT " . $this->fields($fields) . " FROM " . $this->table;

        return $this;
    }

    /**
     * Start an Insert statement into the current table based on the fields and values passed
     *
     * @param $fields
     * @param $values
     * @return $this
     */
    public function insert($fields, $values)
    {
        $setValues = implode(",", array_map("self::placeholder", $fields));

        $this->query = "INSERT INTO " . $this->table . "(" . $this->fields($fields) . ") VALUES($setValues)";
        $this->addBindings($fields, $values);

        return $this;
    }

    /**
     * Start an Update statement on the current table based on the fields and values passed
     *
     * @param $fields
     * @param $values
     * @return $this
     */
    public function update($fields, $values)
    {
        $setValues = null;
        foreach ($fields as $field)
        {
            $setValues .= $this->fieldOperatorValue($field, '=') . ",";
        }

        $this->query = "UPDATE " . $this->table . " SET " . rtrim($setValues, ",");
        $this->addBindings($fields, $values);


        return $this;
    }

    /**
     * Start a delete statement on the current table
     *
     * @return $this
     */
    public function delete()
    {
        $this->query = "DELETE FROM " . $this->table;

        return $this;
    }

    /**
     * Add a where clause to the query based on the field and operator
     *
     * @param $field
     * @param $operator
     * @param $value
     * @return $this
     */
    public function where($field, $operator, $value)
    {
        if (StringHelper::contains($this->query, "WHERE"))
        {
            $this->query .= " AND " . $this->fieldOperatorValue($field, $operator);
        }
        else
        {
            if (!StringHelper::contains($this->query, ["SELECT", "UPDATE", "INSERT"]))
            {
                $this->select("*");
            }
            $this->query .= " WHERE " . $this->fieldOperatorValue($field, $operator);
        }
        $this->addBinding($field, $value);

        return $this;
    }

    /**
     * Returns the results from the created query and bindings
     *
     * @return mixed
     */
    public function get()
    {
        return $this->fetch($this->fire());
    }

    /**
     * Execute the query pre-fetch
     *
     * @return mixed
     */
    public function fire()
    {
        $query = $this->connection->prepare($this->query);
        $query->execute($this->bindings);
        return $query;
    }

    /**
     * Fetches the value(s)
     *
     * @param $query
     *
     * @return mixed
     */
    private function fetch($query)
    {
        if ($query->rowCount() > 1)
        {
            return $query->fetchAll($this->fetchMode);
        }

        //If there is only a single row returned, just return the value
        return current($query->fetch($this->fetchMode));
    }

    /**
     * Bind many fields with an array of keys and an array of values
     *
     * @param $fields
     * @param $values
     */
    private function addBindings($fields, $values)
    {
        foreach (array_combine($fields, $values) as $field => $value)
        {
            $this->addBinding($field, $value);
        }
    }

    /**
     * Adds a binding
     *
     * @param $field
     * @param $value
     */
    private function addBinding($field, $value)
    {
        $this->bindings = array_merge($this->bindings, [":$field" => $value]);
    }


    /**
     * Returns the created SQL statement
     *
     * @return mixed
     */
    public function toSql()
    {
        return $this->query;
    }

    /**
     * Creates the correct string for the SELECT statement
     *
     * @param $fields
     *
     * @return string
     */
    private function fields($fields)
    {
        if (is_array($fields))
        {
            return ($fields === "*" || StringHelper::contains(":", $fields)) ? $fields : implode(', ', $fields);
        }
        return $fields;
    }

    private function placeholder($field)
    {
        return ":" . $field;
    }

    /**
     * Creates the correct string for the WHERE clause
     *
     * @param $field
     * @param $operator
     *
     * @return string
     */
    private function fieldOperatorValue($field, $operator)
    {
        if (strtoupper($operator) === 'LIKE') $this->bindings[":$field"] = "%" . $field . "%";

        return $field . " " . $operator . " " . $this->placeholder($field);
    }

    /**
     * Close the QueryBuilder's database connection
     */
    public function close()
    {
        $this->connection = null;
    }

}