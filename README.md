# README #

This is my ATTEMPT at understanding some of the fundamental concepts of MVC and framework development.

It has basic routing, templating engine, dependency injection (for controllers), MySQL based sessions with encryption, and various string and array static helper methods.

It needs to expanded dependency injection to allow for unit tests.

**This uses SecureRandom, Collection, and Encryptor from Laravel 5.0