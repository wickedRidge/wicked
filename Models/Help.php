<?php

namespace Models;

use Wicked\Model as BaseModel;

class Help extends BaseModel {

    protected $table = 'help';

    public function __construct()
    {
        parent::__construct();
    }

}