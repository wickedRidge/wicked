<?php

use Wicked\Base\Container;

$router = Container::resolve('router');

$router->addRoute('home', 'GET', 'HomeController', 'index');
$router->addRoute('error', 'GET', 'ErrorController', 'index');
$router->addRoute('help', 'GET', 'HelpController', 'index');
$router->addRoute('help/other', 'GET', 'HelpController', 'other');
$router->addRoute('help/other/{$question}', 'GET', 'HelpController', 'other');