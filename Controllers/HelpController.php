<?php

namespace Controllers;

use Wicked\Base\Container;
use Wicked\Http\Controller;
use Models\Help;
use Wicked\Http\Request;

class HelpController extends Controller
{

    public function __construct(Help $help)
    {
        parent::__construct();
        $this->help = $help;
    }

    public function index()
    {
        $this->view->render('help/index');
    }

    public function other(Request $request)
    {
        $help = $this->help->select('question')->where('id', '=', 1)->get();
        $this->view->with('help', $help)->render('help/other');
    }
}
