<?php

namespace Controllers;

use Wicked\Http\Controller;
use Wicked\Http\Request;

class ErrorController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->view->with('error', 'Something went wrong... : (')->render('error/index');
    }
}