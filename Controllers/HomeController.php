<?php

namespace Controllers;

use Wicked\Http\Controller;
use Wicked\View;

class HomeController extends Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->view->render('home/index');
    }
}